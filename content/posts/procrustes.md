---
title: "Stereotypes"
date: 2020-04-09T11:37:22+01:00
draft: false
tags: ["outliers", "short & random"]
---
![
Theseus fighting Prokrustes. Surround of the tondo of an Attic red-figured kylix, ca. 440-430 BC. Said to be from Vulci.
](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Theseus_Minotaur_BM_Vase_E84_n3.jpg/1200px-Theseus_Minotaur_BM_Vase_E84_n3.jpg)

Stereotypes are a [Procrustean bed](https://en.wikipedia.org/wiki/Procrustes). Once established, individuals are pressured to conform to a group mean.

Very few people are content to let outliers be: either they try to explain why the refractory datapoint isn't really an outlier, or argue that it was never really part of the group under consideration (the [No True Scotsman](https://en.wikipedia.org/wiki/No_true_Scotsman)  maneuver).

