---
title: "Running the Jewels"
date: 2020-05-02T16:27:17+02:00
draft: false
tags: ["sports", "storytelling"]

---

I prefer the storyline of running plays to passing plays: 

> A man has obtained a great treasure — can he evade his pursuers and get home in time? 

![Tecmo Bowl](/img/tecmo-bowl.png)

<!--more-->

It's like the Odyssey — they even call home port the "end zone".

The RB travels to the QB for the ball (his need), encounters obstacles, suffers setbacks, and goes home changed (+6 points).

A passing play, in comparison, shortcuts a lot of the human drama with physics: will the receiver run the correct pattern unimpeded and end up in the right place to catch the object of his desire?

This is partially a matter of perspective: I identify with the running back and his Hero's Journey; from the quarterback's point of view, each possession is a drama in 4 downs.

Even so, I think the running back has a better story. You can't just wait for your destiny to arrive, and you can't pass it off to someone else. You have to run with it.

[![Bo knows drama](http://img.youtube.com/vi/8PBvOxicz-0/0.jpg)](http://www.youtube.com/watch?v=8PBvOxicz-0)
<span style="color:rgb(64,224,208)">*Bo knows drama*</span>