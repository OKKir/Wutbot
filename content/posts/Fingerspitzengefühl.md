---
title: "Fingerspitzengefühl"
date: 2020-01-15T22:32:40+01:00
draft: false
tags: ["human-friendly", "metis", "science"]

---

![The hospital in Thessaloniki, 1917-1918](https://upload.wikimedia.org/wikipedia/commons/8/8f/Laboratory%2C_the_hospital_in_Thessaloniki.IMG_0446.jpg)


## Lab hands 

As you spend time working at a laboratory bench, you develop a second sense --- we called it *lab hands*--- for the way your body interfaces with and manipulates your equipment. So far, so ordinary: this is a specialized form of the proprioception shared with athletes, dancers, and anyone else who develops expertise in a particular repertoire of movements.

But the most interesting implication of *lab hands* is not about what the hands are doing.

<!--more-->

The bench presents a  different cognitive environment than the desk or the computational lab, but it is a mistake to envision the desk as representing *theory* and the bench as the home of *praxis* : in reality, the two environments are deeply intertwined in an iterative process of discovery. The bench acclimates the scientist to the constraints presented by reality, both refining existing theories and spurring the creation of new hypotheses. Seasoned lab hands may one day develop a sort of scientific *metis*.

![Metis, maybe](https://upload.wikimedia.org/wikipedia/commons/5/53/Winged_goddess_Louvre_F32.jpg)

## Metis and Outsourcing

Theory and praxis are inextricably linked at the level of the individual researcher. Design and manufacturing are similarly linked at the scale of the firm, as former Intel Chief Andy Grove has [pointed out.](https://webcache.googleusercontent.com/search?q=cache:du6MWMtE9wQJ:https://www.bloomberg.com/news/articles/2010-07-01/andy-grove-how-america-can-create-jobs)

When a company outsources its manufacturing, it misses out on all the innovation that happens during the scaling and production phases. Those innovations are the *pre-adaptations* that position a company to capitalize on the inevitable *exaptations* of that technology as it is out to new uses.

if too many firms try to sever design from manufacturing, it can have a detrimental effect on the constructive fitness of an entire technological ecosystem. Andy Grove thought that this was hobbling the high-tech manufacturing sector in Silicon Valley, using the example of the U.S.'s lost competitive edge in battery technology. We missed the pivot to mobile, to electric cars, and to solar, and Metis moved to Asia.

## Remaining alert to the future

![Otto Pilot](/img/Airplane_screenshot_Haggerty_Nielsen.jpg)

At the same time that globalized outsourcing has Metis putting on her traveling coat, increased reliance on automation is outsourcing praxis to machines.  

We are now building algorithmic systems that strive to render human implicit knowledge obsolete. 

The consequences at t+1 of removing humans from the design loop are not yet clear. In the near to medium term, we might find ourselves stuck in a local minimum akin to that of not-fully-autonomous self-driving cars: trying to keep operators alert enough to react to infrequent weird catastrophic failures.

In the longer term, we risk removing humans from the innovation chain building the physical and social technology of the future. Is it possible to build human-aligned technology without humans?




