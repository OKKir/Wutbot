---
title: "The dark forest internet"
date: 2023-06-18
draft: false
tags: ["models", "economics", "AI"]
cover: "img/A_dark_forest_-_geograph.org.uk_-_901640.jpg"
---
 

What happens when attention becomes an even scarcer resource?

<!--more-->

Much of the internet's utility derives from its ability to make hidden expertise more legible. This function, plus differing proportions of sociality and gamification, is behind the  success of sites  like Stack Overflow, DeviantArt, Reddit, Artstation, and various fora. It is explicitly part of the promise of sites like Quora and Epicurious.

These are the sites that are the most-at risk from the deluge of AI-generated inexpert material, and the unattributed regurgitation of their bread-and-butter as tokens.

<center>
<figure style="text-align:center;">
<img src="/img/Attention_Deficit_(Wale_album_-_cover_art).jpg" width="300px"><figcaption>pondering AI's attention deficit</figcaption></figure></center>

This creates a dilemma for the data commons: folks inclined to contribute to a site for kudos, upvotes, or [whuffie](https://bitcoinwiki.org/wiki/whuffie) may be disincentivized to continue to contribute if the site becomes a desert and all attention-givers decamp to ChatGPT.

Maybe rather than a [Dead Internet](https://en.wikipedia.org/wiki/Dead_Internet_theory) we'll have a [Dark Forest](https://en.wikipedia.org/wiki/Dark_forest_hypothesis) internet, where content producers will hide in un-searchable corners of the internet to keep their expert output pure and safe from AI webtrawlers.

<center>
<figure style="text-align:center;">
<img src="/img/
Saint-Aignan_(Loir-et-Cher)._Okapi.jpg" width="300px"><figcaption>not federating on Mastodon</figcaption></figure></center>



