---
title: "My First Post"
date: 2019-11-26T10:38:08+01:00
draft: true
cover: "img/pixelated-polaroid.jpg"
tags: ["test", "test2", "markdown"]
Description: "testing **Description** field"
---

# h1 testing markdown formating

## h2 what does this look like?

### h3 one more level

#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
----

Alt-H2
_____

____

****
----

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

1. First ordered list item
2. Another item
    - Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
    1. Ordered sub-list
4. And another item.


[I'm an inline-style link](https://www.google.com)

Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Inline `code` has `back-ticks around` it.

 
```python
s = "Python syntax highlighting"
print s
```

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.
>
> <em>test</em>
*This text will be italicized in <em style="color: #007acc;">the specified color</em>.*