---
title: Mars is a lab
date: 2024-05-24
draft: false
cover: "img/mars-kicks-up-the-dust-as-it-makes-closest-approach-to-earth-6578ea-1024.jpg"
tags: ["space"]

---

 It looks like the Overton Window has finally caught up with Alina Chan. It is now acceptable to entertain the notion that the [COVID-19 pandemic originated in a laboratory](https://www.nytimes.com/interactive/2024/06/03/opinion/covid-lab-leak.html). 


<!--more-->

Bully for free and open inquiry and all that, but what are the implications for the greater biosecurity landscape? How should we be handling our most dangerous biomolecules?

## Risks and Benefits

It is interesting now to return to the  [2015 Risk and Benefit  Analysis of Gain-of-Function Research](https://osp.od.nih.gov/wp-content/uploads/2015/12/Risk%20and%20Benefit%20Analysis%20of%20Gain%20of%20Function%20Research%20-%20Draft%20Final%20Report.pdf) and its [critiques](https://armscontrolcenter.org/wp-content/uploads/2017/03/An-Analysis-of-Chapter-6-in-Gryphon-Scientific-RA-1-2-2016.pdf) in light of this new  empirical  data.

The report acknowledges at least one inherent difficulty in this type of analysis: the risks are immediate, while the benefits of GoF findings may be realized far in the future. The report proposed to use a 5 year window for  risks, while counting any benefits to research occurring within this window indefinitely, to account for this temporal mismatch. 

It appears that any WIV research potentially responsible for the 2020 pandemic occurred during and after the [2024-2017 US moratorium on GoF research](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC10234839/). If the Wuhan research is responsible for the COVID-19 pandemic, the risks of the research were realized squarely within the 5 year window. The consequences, *if* WIV research is responsible,  would include 
[7 directly attributed million COVID-19 deaths](https://www.google.com/url?sa=t&source=web&rct=j&opi=89978449&url=https://www.worldometers.info/coronavirus/&ved=2ahUKEwiitMTL_cOGAxWW_7sIHU3-Cv8QFnoECCgQAQ&usg=AOvVaw3kNWYzks92mP5xVSU6CwDa), with a [95% interval of 16-28 million deaths when all COVID-induced excess deaths are taken into account](https://en.wikipedia.org/wiki/COVID-19_pandemic_deaths).

The 2015 report estimated that the release of  wild type seasonal influenza strain could result in 4 million deaths, while the release of a strain of similar pathogenicity to  the 1918 pandemic could kill up to 80 million. The authors of the report did not think that there was a significant chance of an accidental SARS-CoV release sparking a global pandemic, and did not provide direct mortality estimates for such an event.

### What about the benefits?

A thorough benefit assessment should include a quantitative analysis of the *unique* benefits of GoF research across various GoF phenotypes and several types of pathogens, both globally and to specific populations. The benefit prospectus in the 2015 report describes expected unique  benefits; and two categories of benefit stand out: rapid development of vaccines, and improved therapeutic efficacy/reduced therapeutic evasion.

When compared with alternative methods for investigating coronaviruses, the 2015 report deemed GoF approaches uniquely capable of investigating aspects of virulence relevant to the development of live attenuated vaccines, identifying viral targets of novel therapeutics,  determining optimal therapeutic doses to avoid anitiviral resistance, and evaluating the potential of broad-spectrum vaccines.

### 10 years on from the GoF moratorium, can we update our priors on GoF utility?

A [2022 study](https://www.frontiersin.org/articles/10.3389/fbioe.2022.966586/full) argues that these potential benefits were not directly realized in the COVID pandemic. The most effective technological components of the SARS-CoV-2 response were next-generation sequencing and mRNA and recombinant adenovirus vector vaccine technologies. The protocol of sequencing viral genomes to identify target sites and creating mRNA vaccines to address those targets sidesteps the need to perform GoF strain development and screening.

An updated risk-benefit assessment would suggest that the risks of CoV research were underestimated, and the benefits over-stated. 

## But what does this have to do with the price of potatoes on Mars?

It is unlikely that even an updated RBA would lead to a complete moratorium on this type of research. We've invested fairly heavily in research infrastructure, and there are a lot of very intelligent and articulate people interested in its continuance. Even if its utility in developing medical countermeasures is not as great as we once believed, it might still be attractive for "biodefense" applications. After all, there are a significant number of people around the world with a very particular set of skills...

<center>
<figure style="text-align:center;">
<img src="/img/Taken1.jpg" width="600px"><figcaption>
"Yes, I can do a serial passage." </figcaption></figure></center>

And, in fairness, one could argue that the GoF moratorium itself stymied research that may have led to a faster or more positive resolution of the COVID-19 pandemic.

<center>
<figure style="text-align:center;">
<img src="/img/mars2.png" width="600px"><figcaption>
Mars attac ... but he also protec </figcaption></figure></center>


Enter the red planet, or as I like to think of it, the solar system's natural BSL-4 facility. What if, instead of building facilities in Boston or Atlanta, we instead built cloud labs on Mars, providing a  steady stream of discoveries, publications, and grant-supported income to its operators back on earth?

<center>
<figure style="text-align:center;">
<img src="/img/Strateos.jpg" width="600px"><figcaption>
This but redder.</figcaption></figure></center>

The  first space pioneers might include hardy virologists and maintenance engineers, separated from population centers by the world's most secure airlock: the vacuum of space. 