---
title: "Elevator pitch"
date: 2020-02-09T11:13:57+01:00
draft: true
tags: ['data science']
---

I use large sets of [known knowns](https://en.wikipedia.org/wiki/There_are_known_knowns) and __known unknowns__ to investigate __unknown unknowns__.

