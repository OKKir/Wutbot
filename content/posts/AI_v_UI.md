---
title: "AI vs. UI"
date: 2020-02-25T22:30:37+01:00
draft: true
tags: ["tech", "AI", "UI"]
cover: "img/minority_report.jpg"
---

I once worked on a project where I had to use a rather finicky robot assembly to perform a chemical pretreatment procedure before a mass spectrometric analysis. Sometimes the robot arm would perform incorrecctly, ruining the time-consuming analysis. I would sometimes fantasize about 


![](img/robot-learning.jpg)<span style="font-size:8px">forcing A disembodied intelligence to assume corporeal form and use inefficient gestural interfaces may be our only hope </span>
