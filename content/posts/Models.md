---
title: "Models"
date: 2020-01-15T23:07:22+01:00
draft: false

tags: ["models"]
---

## All Models Are Wrong


![Henri Rousseau](https://upload.wikimedia.org/wikipedia/commons/f/fa/Surprised-Rousseau.jpg)

> "All models are wrong, but some are useful" - George Box

<!--more-->


>  "Since all models are wrong the scientist must be alert to what is importantly wrong. It is inappropriate to be concerned about mice when there are tigers abroad " - George Box


Some of our most interesting material technology began its use-life as a toy, and our cognitive technology follows the same ontogenetic trajectory.

Toy models are not just about fit, but about generating ideas about
the types of processes that could result in the observations. 

They can also be used to explore
equifinality: spinning up a list of possible paths leading to the observed data.

Simple models --- as toy models usually are --- do not necessarily reflect the belief that nature is simple, but humbly describe the parts of nature we can acces with our limited means of perception. 

![toy tiger](/img/toy_tiger.jpg)