---
title: "Building playgrounds for the imagination"
date: 2024-01-02
draft: false
tags: ["storytelling", "games"]
cover: "img/Fez.jpeg"

---

I've been thinking about how and why some topics, periods, and ideas become fertile ground  supporting  an ecosystem of imaginary worlds, and others do not.

<!--more-->

For example, the works of Tolkien and the period of ancient Rome have been highly generative, as was the conquest of the western United States.  I'd like to see if it is possible to terraform other topics and periods of history so that they bloom as arenas for imaginative play.

I'm particularly interested in the relative dearth of  stories and games around the Underground Railroad in the U.S. After my mother took my children to a UGRR site in my hometown, I found my daughter playing "hide from slavery" under an afghan in her grandparent's  home office.  She was having a fine time, but I bet I'd have trouble getting the neighborhood kids to come over to play "Freedom Seekers & Bounty Hunters"[^1]. On the other hand, I could throw a *Gladiator*-themed party without anyone batting an eye. And idea-starved Hollywood has not yet set a retelling of the Odyssey that opens on a plantation.

### How do you turn a period into a platform?

Creating a memorable experience that supports ongoing imaginative  expansion and replay requires capturing and conveying the distinctive aesthetic and animating question of the time:  can we defend our way of life? is Rome the greatest empire? what does it mean to be a knight?

In fantasy and science fiction writing, this is part of *worldbuilding*. But for historical fiction, these worlds already exist: the task is to draw out their distinctive features and  invite players to inhabit them.

<center>
<figure style="text-align:center;">
<img src="/img/012.romance.tristram.iseult.1909.maurice.lalau.jpg" width="600px"><figcaption>Tristan and Isolde's flight through a forest</figcaption></figure></center>

Role-playing games -- especially social roleplaying games like tabletop RPGs and strategy games -- are a great way of immersing players in a place, time, or question and allowing them to explore how they would respond to its particular challenges. Players can assimilate a lot of information -- and become incentivized to seek out more, post-game -- while enjoying the freedom to navigate their own thoughts and feelings about the experience.

I recently had a lot of fun playing the [Sorcery!](https://www.inklestudios.com/sorcery/) series by Inkle Studios, a digital conversion of a gamebook series by Steve Jackson. The game designers cleverly used a normally dispreferred emergent mechanic -- "save scumming" -- to allow players to experience more of the story, incentivizing greater risk-taking and exploration of the game world.  It struck me that this type of interactive fiction experience could be a good model for a historical fiction game...

<center>
<figure style="text-align:center;">
<img src="/img/harriet-tubman-underground-railroad-kolongi-brathwaite.jpeg" width="600px"><figcaption> Freedom seekers' flight through a forest,
<a href="https://www.blackartdepot.com/products/harriet-tubman-underground-railroad-kolongi-brathwaite">Kologni Braithewaite</a>
</figcaption></figure></center>

So I'm designing a narrative adventure game where the player character is a freedom seeker navigating the Underground Railroad in the 19th century United States, and working with [experts on the period](https://www.restoreclevelandhope.com/) to weave in accurate historical details and narrative.

We're calling the game "[Station Hope](/station-hope.html)", after the Underground Railroad's code designation for Cleveland, Ohio. I hope it can be a small step toward a livelier understanding of this historical period. 

<center>
<figure style="text-align:center;">
<img src="/img/
LotrProject_MiddleEarth_Map_High_Res.jpeg" width="600px"><figcaption>possible model for an Underground Railroad quest map?</figcaption></figure></center>


[^1]: To be fair, "Cowboys and Indians" is probably deprecated, too.
