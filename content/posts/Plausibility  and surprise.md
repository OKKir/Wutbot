---
title: Plausibility, ridiculousness, and surprise
date: 2023-11-17
draft: false
tags: ["impro", "storytelling"]
cover: "img/madtv-sopranos.jpg"
---

Will Sasso doesn't look anything like Arnold Schwarzenegger. Or Steven Seagal. He *does* kinda look like James Gandolfini. But his impressions of all three actors are equally good. Why?

<!--more-->

I think Sasso's style of impressions hit the sweet spot between *plausibility* and *ridiculousness*. His Arnold regularly lets loose with a sort of Tyrolean gurgle that *sounds* like a noise Arnold would make -- even though I'm pretty sure's never uttered that sound. And his lumbering Steven Seagal takes his characteristic stiffness and squinting to its logical extreme 

<center>
<figure style="text-align:center;">
<img src="/img/sasso-seagal.gif" width="400px"><figcaption>surprise</figcaption></figure></center>

These moves are not real, but they're believable, and it's a delight when Sasso pulls it off: you know they aren't accurate, but they *should* be. And the viewer is surprised to recognize the subject of the impression as if it was performed with perfect fidelity. It's like a magic trick that keeps working.


<iframe width="560" height="315" src="https://www.youtube.com/embed/RZzHI0M5Gis?si=gW8fPT5pDkZYviUA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

