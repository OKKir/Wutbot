---
title: Honest Technology
date: 2024-06-08
draft: false
tags: ["technology", "reasons to be cheerful"]
cover: "img/applemac.jpg"
---

An honest piece of software is like an honest politician: when it's bought, it stays bought.


<!--more-->

Consumer software is stampeding toward the subscription model. It's not surprising -- why sell something once when you can sell it multiple times to the same customer?

[Adobe](https://news.ycombinator.com/item?id=40607442) is in the news for exploring new frontiers in software-as-a-service, extending their subscription model to claim access to its customers' creative output.


<center>
<figure style="text-align:center;">
<img src="/img/rumpelstiltskin.jpg" width="600px"><figcaption>Drafting the Adobe TOS</figcaption></figure></center>


The villains are many. I'm here to sing about the heroes.

 I've been consistently pleased with [Scrivener](https://www.literatureandlatte.com/scrivener/overview), a one (payment)-and-done tool for longform writing with one of the most consumer-friendly trial periods I've seen: a 30-day trial that only counts days on which you  boot the software. Nice. 


I'm still loving [Obsidian](https://obsidian.md/), a local-first markdown pkm tool that only charges personal users to sync and publish content. [^1] Huzzah!

And for markdown aficionados who want a stylish and streamlined editor, rather than an IDE, [Typora](https://typora.io/) makes drafting a pleasure.

<center>
<figure style="text-align:center;">
<img src="/img/accentuate.jpg" width="600px"><figcaption>Eliminate the negative</figcaption></figure></center>

I'm considering switching back to booting Linux when support for Windows 10 ends in October 2025. The only reason I switched to Windows in the first place -- the only reason I've ever used Windows -- is to play computer games. Or rather, to have the *option* of playing certain computer games. Apparently the mere contemplation of my Steam library[^2] is pleasure enough.

<center>
<figure style="text-align:center;">
<img src="/img/myciv.PNG" width="600px"><figcaption>Also plays on my 2016 Macbook Air...</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/myskyrim.PNG" width="600px"><figcaption>The thought of revising my mod install order for a 12 year-old game I haven't played since Covid has lashed me to the stumbling corpse of the Windows OS</figcaption></figure></center>


The open-source and local-first software landscapes have grown to the point where it is now possible to have a first-class computing experience running free-range software ... as long as you're not a hardcore or multiplayer gamer, and you have something of a fetish for troubleshooting driver issues.

 As someone who likes her computer games free of other human intelligences  and thrills to typing `sudo apt install linux-headers-$(uname -r)`, everything's turning up Milhouse.


[^1]: I'd include [Logseq](https://logseq.com/) here, too, but as far as I can tell they don't have a pricing model yet

[^2]: Every time a Steam game update downloads I  get more nervous about the move from physical games media