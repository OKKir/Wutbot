---
title: "Taylor Swift is a Schelling Point"
date: 2024-03-01
draft: false
cover: "img/taylor-swift.jpg"
tags: ["culture"]
Description: 
---


The Matthew effect: pop music edition

<!--more-->

> For to every one who has will more be given, and he will have abundance; but from him who has not, even what he has will be taken away.  — Matthew 25:29, RSV.

## Rivalries & rooting interests

*The Beatles vs. The Rolling Stones* 

*Prince vs. Michael Jackson*

*Nirvana vs. Pearl Jam*

*Britney vs. Christina*

Pop music eras have their idols, and their tribes. These are usually weak allegiances driven by the narcissism of small  differences: most MJ fans sang along to "Purple Rain" in the car.

<center>
<figure style="text-align:center;">
<img src="/img/Prince-MIchael-Jackson.jpg" width="600px"><figcaption>
Choose your fighter. </figcaption></figure></center>

But the tribes served a function: they provided ready-made common ground. 

Now, in a more fractured cultural environment with a Spotify stations serving tide-pool sized listening niches, finding this common ground can be more difficult. But I think Taylor Swift is evidence that we still want it.

## Creating community

Tay-Tay is very legible, and very prominent -- a perfect Schelling point in a crowded music landscape. I think she enjoys a particular type of preferential attachment, where young women enter the Swiftie fandom because of rather than in spite of her popularity to form common ground with each other.  

This confounds Gen X and Geriatric Millennials, who sought out more obscure bands for their tribal allegiances. But they still benefitted from the cultural safety net of a more unified musical background. You could assume others had at least some incidental exposure to popular radio hits, and if all else failed, you could still make small talk about Mariah Carey or  Bon Jovi.

<center>
<figure style="text-align:center;">
<img src="/img/swift-coke2.jpg" width="600px"><figcaption>
Every restaurant serves Coke. </figcaption></figure></center>

There are other fandoms, of course -- K-Pop comes to mind. But the barriers to entering the land of T-Swizzle are especially low, so she becomes the musical lingua franca for young women seeking a  social experience. The auto-biographical nature of her songs fosters a sense of intimacy, creating the feeling that her fans are a network of friends with a famous bestie.

The last musician to run this playbook was probably Beyoncé. But her millennial audience no longer needs that community as much as they once did.

<center>
<figure style="text-align:center;">
<img src="/img/kpop.jpg" width="600px"><figcaption>
Too much choice </figcaption></figure></center>

People outside of Taylor Swift's fandom (and I am one of them) sometimes sneer that her songs are bland, interchangeable, indistinct. I think this is a particularly modern misunderstanding of her craft. Her music relies heavily on *melody* and *lyricism* for its power. In this way, she's beating against the current trend toward [less melodious](https://www.youtube.com/watch?v=K0Vn9V-tRCo) pop music, and the migration of dense first-person  lyrical content to the rap genre. 

<center>
<figure style="text-align:center;">
<img src="/img/sting.jpg" width="600px"><figcaption>
Taylor Swift : Zoomers :: Sting : Boomers</figcaption></figure></center>

Taylor Swift's music doesn't approach the lyrical density of artists like Kendrick Lamar, but her narrative song texts serve up a perfect substrate for community-building exercises in lyrical exegesis. It's good to see  youngsters embracing the fun of analyzing cultural products. It's very Lindy.

<center>
<figure style="text-align:center;">
<img src="/img/tia3.jpg" width="600px"><figcaption>
Remember when we spent a whole summer analyzing every frame of this video like it was a Dead Sea scroll?</figcaption></figure></center>

The big question is whether Taylor can continue her reign as the [Grand Central Station clock](https://nav.al/schelling-point) of pop culture. I think her community-building function will keep the preferential attachment machine rolling through multiple life stages, at least for the Zoomer cohort,  *if* she continues to build intimacy with her fanbase. Can she chart with a ballad about potty training? 

I think she can if she wants it.

<center>
<figure style="text-align:center;">
<img src="/img/potty-training.jpg" width="600px"><figcaption>
Stay Stay Stay</figcaption></figure></center>