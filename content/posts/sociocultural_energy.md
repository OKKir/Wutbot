---
title: "Sociocultural Energy and Entropy"
date: 2020-05-01T11:06:02+01:00
draft: false
tags: ["rules", "short & random"]
---

![Anton Graff - Selbstbildnis mit Augenschirm - Google Art Project](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Anton_Graff_-_Selbstbildnis_mit_Augenschirm_-_Google_Art_Project.jpg/1200px-Anton_Graff_-_Selbstbildnis_mit_Augenschirm_-_Google_Art_Project.jpg)

Sociocultural rules use energy to mediate society's relationship to natural law.

Biology and other sciences are not *prescriptive* of social rules, but are rather the *accounting* system tracking the amount of energy required to shift human society a distance *X* from the relevant natural law.
