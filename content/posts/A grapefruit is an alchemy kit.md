---
title: A grapefruit is an alchemy kit
date: 2023-01-13
draft: false
tags: ["science", "alchemy"]
cover: "img/morrowind_alchemy_kit 1.jpg"
---

> "A grapefruit is just a lemon that saw an opportunity and took advantage of it" -- Oscar Wilde

<!--more-->

<center>
<figure style="text-align:center;">
<img src="https://upload.wikimedia.org/wikipedia/commons/b/be/Grapefruit_free.jpg" width="300px"><figcaption>More powerful than you could possibly imagine</figcaption></figure></center>

Grapefruit-drug interactions are common enough to warrant [their own Wikipedia entry](https://en.wikipedia.org/wiki/Grapefruit%E2%80%93drug_interactions#Affected_drugs)

Not only was grapefruit "discovered" by accident (an accidental cross between a sweet orange and a pomelo in 18th c Barbados), its potential to interact adversely with medication was as well, during a 1991 study of drug-alcohol interactions using grapefruit juice to mask the taste of alcohol.

Compounds within grapefruit inhibit enzymes involved in drug metabolism. This inhibition can have opposite effects depending on whether the inhibited enzymes activate or inactivate the particular drug compound. So the same glass of grapefruit juice may turn the correct dose of statins into an excessive dose, and a proper dose of an antihistamine into an insufficient dose.

<center>
<figure style="text-align:center;">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/6bd7.jpg/250px-6bd7.jpg" width="300px"><figcaption>CYP3A4, absolutely mogged by grapefruit</figcaption></figure></center>

So a grapefruit is a bit like a multipurpose tool for organic alchemy: reducing the effectiveness of some drugs and supercharging others. We crossed a couple of citrus fruits in the botany subgame and ended up unbalancing the alchemy  mini-game.

<center>
<figure style="text-align:center;">
<img src="https://images.uesp.net/thumb/9/99/MW-quest-A_Falling_Wizard_02.jpg/800px-MW-quest-A_Falling_Wizard_02.jpg" width="500px"><figcaption>Grapefruit casts Improve Acrobatics and Slowfall at the same time, like a boss</figcaption></figure></center>
