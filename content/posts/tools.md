---
title: "Tools"
date: 2020-01-16T09:29:45+01:00
draft: false
tags: ['technology', 'tools']
cover:   
---

## Tools, Tasks, and Collaboration

![Blombos point](/img/Blombos_point.jpeg) <span style="font-size:8px">*image from Vincent Mourre / Inrap*</span>

I've noticed something about working in groups:
The more discrete the task and the fewer people you have to collaborate with, and the better and broader your selection of potential tools. 

<!--more-->

In effect, large collaborations limit your degrees of freedom with respect to tool choice. This is trivially true for reasons of coordination costs before you even consider network effects, switching costs, differences in competence, or fanboy tech factionalism. For groups whose work requires spcialized tools about which people can become opinionated, it's a good reason to stay small as long as possible.

