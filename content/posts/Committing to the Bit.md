---
title: Committing to the Bit
date: 2024-04-20
draft: true
tags: ["culture","art", "television"]
cover: "img/the-witcher2.png"
---


Thoughts on watching *The Witcher* as a prelude to reading the books.

<!--more-->

I love the sword and sorcery genre, which I loosely define as high fantasy in a disordered world. I'm about ready to dive into Andrzej Sapkowski's *Witcher* series, which won the coin toss over Michael Moorcock’s *Elric of Melniboné* on account of being neatly ordered in paperbacks that fit in my gym bag. 

<center>
<figure style="text-align:center;">
<img src="/img/sorcery_village.jpg" width="600px"><figcaption>Go on, tell 'em about your magic system</figcaption></figure></center>

I'm looking forward to experiencing a fantasy series drawing on central European folklore. Perhaps it will rouse me from my Tolkeinian slumber. 

So I decided to pregame by  watching the first few episodes of the Netflix series, to get into T H E  A E S T H E T I C.

I don't think this is the entree into Temeria I was looking for.  The show seems like a better reflection of the streaming teevee era's neuroses than the book series it draws upon.

Weird anachronism in speech and affect, inconsistent psychological motivation, a blunting of the unique properties of the source material 

<center>
<figure style="text-align:center;">
<img src="/img/cavill-witcher.jpg" width="600px"><figcaption>Cleanup on Season 2</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/Mcdowell.jpg" width="600px"><figcaption>Is this the showrunner?</figcaption></figure></center>