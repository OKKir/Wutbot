---
title: Science fiction as a lens on scientific progress
date: 2023-10-13
draft: false
tags: ["science", "sci-fi","testable hypotheses"]
cover: "img/ritornel cover.jpeg"
katex: true
---


Science fiction uses the future as an arena to explore and extrapolate from the problems of the present, with technological  change and sociocultural change alternating in the role of dependent and independent  variables. A broad overview of the trends in 20th and early 21st century sci-fi (or, more honestly, a quick once-over of the books that came to mind when I  thought this up in the shower) suggests that the interplay between these variables in our own time determine the predominant  "mood" of that era's sci-fi.

<!--more-->
## Cultural change meets technological change

First, a note on terms and timeline [^1].

### 20th Century Science Fiction

__Pulp Era:__ 1920s-1930s (*Amazing Stories*, *Weird Tales*,  *Metropolis*)

__Golden Age:__ 1938-1946 ( the *Foundation* series, the *Lensman* series,  *The Martian Chronicles*, *Childhood's End*, *Starship Troopers*, *Star Trek* )

__New Wave:__ 1960s-1970s (*2001: A Space Odyssey*, *The Dispossessed*, *Trouble on Triton*, *Stand on Zanzibar*, *Ubik*, *The Drowned World*, *Dune*)

__Cyberpunk:__ 1980s-1990s (*Mirrorshades*,  *Neuromancer*, *Ghost in the Shell*,  *Snow Crash*, *Akira*, *Blade Runner*)  

<center>
<figure style="text-align:center;">
<img src="/img/foundation.jpg" alt="Foundation" width="300"> <figcaption>Source:<a href="https://www.flickr.com/photos/uflinks/4955877639">Flickr</a></figcaption> 
</figure>
</center>

I'd breezily abstract this as an age of fiction focused on gadgetry and sensational adventures ➡️ an age of  worldbuilding, space opera,  and  the use of technology to solve human problems ➡️ an age exploring  the cultural and psychological correlates of technological change ➡️ an age that focuses on the personal computer -- and later, computer networks -- as the central technologies of interest.

The first three eras embrace a larger temporal scale than cyberpunk, which tends to focus on the near future.  

I think it's also fair to say that the Pulp and Golden ages  are generally more optimistic in tone than the  New Wave, where entropy is a recurring theme,  and the cynical tone and dystopian settings common to the Cyberpunk years. 

Of course there are exceptions:  near-future sci-fi, often set after an atomic apocalypse, occurs even in the Golden Age, as do cautionary tales and dystopias.  And the New Wave describes utopias as well as dystopias, sometimes with hints of magical realism that produce a sense of wonder akin to that of space opera. These are patterns rather than rules.

<center>
<figure style="text-align:center;">
<img src="/img/solaris-vintage-movie-poster-original-polish-a1-23x33-5685.jpeg"width="300">  <figcaption>Source:<a href="https://fontsinuse.com/uses/46111/solaris-1972-polish-movie-poster"> Fontsinuse.com</a></figcaption></center>


The pattern I'm most interested in relates the rate of technological change to the rate of cultural change during each sci-fi era.  

### The pattern

The Shower Thought in full:

🌞Golden Age science fiction is the result of  the velocity of technological  change  significantly exceeding  the velocity of social change.



$$
f′(tech \, change) >> f′(cultural \, change)
$$

where

$$
f′′(tech \,change) > 0 
$$

$$
f′′(cultural \,change)
\gtrsim 0 
$$


🌊When the velocity of cultural change began to increase, but remained slower than the rate of technological change, New Wave science fiction was born. 


$$
f′(tech\, change) > f′(cultural \,change)
$$

but

$$
f′′(cultural \,change) \gtrapprox f′′(tech \, change)
$$

where

$$
f′′(tech \,change)
\gtrsim 0 
$$

$$
f′′(cultural\,change) > 0 
$$


🤖When the velocity of technological change slowed somewhat, but still exceeded the velocity of cultural change, the Cyberpunk era began. 

$$
f′(tech\, change) \gtrapprox f′(cultural \,change)
$$

but 

$$
f′′(cultural \,change) > f′′(tech \, change)
$$



where

$$
f′′(tech \,change) < 0 
$$

$$
f′′(cutural \,change) > 0 
$$

<center>
<figure style="text-align:center;">
<img src="/img/weyland-yutani.png" width="400" ><figcaption>Source:<a href="https://fontsinuse.com/uses/35613/weyland-yutani-corp-logo-and-slogan-in-aliens">fontsinuse.com</a>></figcaption></figure></center>


👨‍🚀And when the velocity of technological change slowed or stagnated enough to be overtaken by the  apparent velocity of cultural change, we began to see the emergence of a genre (not really an age) of Competence Porn (*The Martian*, *Dr. Who*, *Firefly*).


$$
f′(tech\, change) < f′(cultural \,change)
$$

and 


$$
f′′(cultural \,change) > f′′(tech \, change)
$$

where


$$
f′′(tech \,change) < 0 
$$

$$
f′′( cultural \,change) > 0 
$$


The [Competence Porn](https://web.archive.org/web/20160331174902/http://www.wired.com/2009/10/admit-it-you-love-competence-porn-too/) trend is not original to the science fiction genre. The thread wends its way through much of  contemporary pop culture, popping up in  on the small screen  in shows like *Sherlock*, *Leverage*, and *House*. It's also not entirely new:  competence porn may be native to military sci-fi; *Star Trek:TNG*  is a standout modern expression. It just seems *prominent* in the current science fiction zeitgeist.

<center>
<figure style="text-align:center;">
<img src="/img/The_Martian_(Weir_novel).jpg" width="250"></figure> 
</center>


### Potential  Influences
 
 It is as if the readers and writers of science fiction are most sensitive to the acceleration or deceleration of technological progress and cultural evolution, and are  constantly comparing the graphs of these second derivatives. 

I think it's also interesting to note that the velocity *and* acceleration of technological change were probably both greatest in the Pulp  and Proto Sci-fi (late 19th-early 20th c, H.G. Wells & Jules Verne) Ages. This raises the question of how much sci-fi authors and readers are integrating over their lifetime experience. Do early childhood expectations affect the mood of a sci-fi age?  

The velocity of cultural change is probably greatest right now, although I'm not sure where we are on the acceleration curve. I suspect *acceleration<sub>cultural</sub>* was greatest in the period following the widespread availability of oral contraceptives to about the late 1970s.  That might create an expectation in children raised in that era of continuing rapid social evolution. I don't think they'd be disappointed, with the collapse of birth rates worldwide, changes to marriage laws, various enlargements and contractions of the average westerner's  circle of empathy, the imposition and abolition of the One Child policy, the fall of Communism, etc. Are Gen X writers producing work that centers sociocultural themes? Does the crop of science fiction produced between the mid 1990s and today mirror or echo the themes and preoccupations of the earlier  New Wave?

On the other hand, *acceleration<sub>technological</sub>* is probably at a local minimum  at present.  Are readers clamoring for near-future science fiction with  familiar technological milieus? Will the writers of the 2040s produce more "tomorrow fiction"[^2]? 

<center>
<figure style="text-align:center;">
<img src="/img/rpo_us.jpeg" width="400px"><figcaption>Source: <a href="https://fontsinuse.com/uses/9994/ready-player-one-by-ernest-cline-random-house">fontsinuse.com</a></figure></center>


>  "Ten year‐olds do not read the scientific literature." - Carl Sagan

Ten year olds might not read *Science* or *Nature*, but they do assimilate the prevailing cultural and technological winds of their developmental years, and use it to calibrate their expectations.  Perhaps if advances in energy, rocketry, and AI technologies  catalyze another tech boom,  Zoomer scifi will be dominated by near-future cyberpunk cozies with highly competent protagonists.

<center>
<figure style="text-align:center;">
<img src="/img/Enders_game.jpeg" width="300px">
</figure>
</center>




[^1]:   [The history of science fiction](https://en.wikipedia.org/wiki/History_of_science_fiction)

[^2]:  Isaac Asimov, in *Nightfall, and Other Stories* [Wikipedia](https://en.wikipedia.org/wiki/Golden_Age_of_Science_Fiction)