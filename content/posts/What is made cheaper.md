---
title: "What is made cheaper"
date: 2023-05-13
draft: false
tags: ["models", "economics", "AI"]
cover: "img/sd-02.png"
---

*Stable Diffusion, Ghibli LORAS: Steampunk industrial revolution factory interior,  brass, copper, pipes, steam, gears, energy*


> If you want to predict the impact of a technology, try to imagine what will be made cheaper

<!--more-->

Powerful technologies make lots of other things cheaper. For example,  electricity made illumination, machinery operation, heating, and cooling cheaper, enabling a massive number of further inventions and productivity increases. 

This is an interesting exercise to apply to AI: what do image generators and LLMs make cheaper? 
If data provenancing is improved, "search" would be one answer. Imagine using an image or test as a `finger` protocol to artists or experts!

A less positive reply would be "bullshit": we can already see AI being used to deaden the internet with mindless SEO-optimized copy and clog teachers' inboxes with stiffly-written 5-paragraph essays.  A more precise response might be "the appearance of attention". People use AI to simulate caring about stuff, like emails to subordinates, or assignments.

I don't know how sustainable this huge and ongoing discount on giving a rat's a**  will be, but it does suggest that the value of genuine attention will rise.  

> "You can see the computer age everywhere but in the productivity statistics" - Robert Solow

<center>
<figure style="text-align:center;">
<img src="/img/robert-solow1.jpg" width="300px"><figcaption>Robert Solow</figcaption></figure></center>

