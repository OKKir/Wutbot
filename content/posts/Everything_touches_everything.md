---
title: "Everything Touches Everything"
date: 2020-01-16T22:56:10+01:00
draft: false
tags: ["networks", "graphs"]
---


![Partial map of the Internet based on the January 15, 2005 data found on opte.org. By The Opte Project](/img/720px-Internet_map_1024.jpg)


> "everything touches everything." - Jorge Luis Borges

<!--more-->

## Linked

Reading *Linked* by physicist and network theorist Albert-László Barabási, I was struck by a diagram of a directed network:

![](/img/graph_islands.png)

According to the author, this picture represents a basic fact about the structure of directed networks, from the World Wide Web, to academic citation networks, to food webs, where links between nodes can only be traversed in a particular direction. 

All directed networks have these same components: a central core with *in* and *out* continents, loosely attached tendrils, and detached islands.

The existence of the central core, its flanking continents, and the islands is unsurprising---they put the *net* in *network*--- but my eyes were drawn to the islands. Their presence seems like a refutation of the claim made earlier in the book that *"everything touches everything"* Could they really be so isolated?

In 2002, when *Linked* was published, these islands probably represented various personal pages, maybe a few blogrings around an obscure fandom. Without Google to act as an index to the web, they floated along carefree in their obscurity, visited only by persons with a sincere interest in their subject or an offline relationship with their maintainer. Sparsely populated, largely pleasant, and ignored by the mainland.


(It is interesting to consider what their analogues in natural systems might be. Maybe uninhabited islands in the Philippines, or off the coast of Madagascar?)

This brings up the question of precisely which flows diagrams such as the one above capture about the networks they describe. One can map the electronically-mediated connections between sites on the World Wide Web, but that doesn't necessarily reveal how each instance of discovery works. Entry to some of these isolates might have followed a personal invitation from the site owner after the utterance of some shibboleth, or a footnote in a 'zine. In any case, this simplified map does not represent the complete set of interactions needed to understand how the system of discovery and referral works. The 2002 world wide web was not a closed system.


## Directed networks and the social web

Since 2002, the social layer of the web has shifted from the blogosphere to gated social media apps with explicit network structures. Facebook is described as an undirected network because of the bidirectional nature of "friend" relationships on the platform. Twitter and Instagram, which allow unreciprocated follower->followee connections, have mixed directed and non-directed components. 


But these more formal descriptions of network structure elide important features of how the networks are experienced by their users. For less active participants on non-directed networks like Facebook, and for low-follower accounts on mixed networks like Twitter, the user experience resembles a directed network: the focal user shouts into a void while being inundated with communications from users with higher *eigenvector centrality* (more connections to other highly-connected users) inundate their feed. For small accounts, Twitter, especially, may feel like receiving a constant stream of power-user propaganda and reminders of their relatively lower  status. 

![twitter graph by Tony Hirst](/img/twitter_graph_ex.jpg)

The mutual nature of friend relationships on Facebook makes the network appear superficially more egalitarian. But algorithmic manipulation of the feed in the service of ad sales  has shifted the network's practical focus from bidirectional connections between users to Whatever Will Drive Engagement. Facebook's culture of relentless A/B testing has turned much of the world into a social science laboratory, and one of its most replicated findings is that negative emotions keep users glued to the platform. Accordingly, the default Facebook feed has been transformed into a steady drip of strangers' leaking id.

![Facebook network diagram](https://upload.wikimedia.org/wikipedia/commons/9/90/Kencf0618FacebookNetwork.jpg)

## How to be an island


> "everything touches everything." - Jorge Luis Borges

The Borges quote probably captures something real about the physical---and metaphysical--- world, something that is also echoed in John Donne's famous verse:

><span style="font-family:Baskerville">No man is an Iland, intire of itselfe; every man
is a peece of the Continent, a part of the maine;
if a Clod bee washed away by the Sea, Europe
is the lesse, as well as if a Promontorie were, as
well as if a Manor of thy friends or of thine
owne were; any mans death diminishes me,
because I am involved in Mankinde;
And therefore never send to know for whom
the bell tolls; It tolls for thee.</span>
>
><span style="font-family:Baskerville">MEDITATION XVII
>Devotions upon Emergent Occasions
>John Donn</span>


Juxtaposing these ideas with a discussion of web technologies subtly implies that ubiquitous connection on multiple levels is natural, even desirable. This may indeed be the case for the natural webs of matter, reason, and sentiment that embrace us as embodied beings. But is it necessarily true of the artificial communication networks we create? 


Is it possible to retain one's natural connections to the rest of the world and selectively prune the electronic? I suspect that *curation*, *decentralization*, and *selective multiplexity* are the keys to becoming an island.

![the goal](/img/desert_island.jpg)


*To be Continued*