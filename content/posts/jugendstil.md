---
title: "Jugendstil"
date: 2020-02-24T22:13:50+01:00
draft: false
tags: ["art", "technology"]
cover: "img/vmfa_magnolia_lamp_off.jpg"

---

I recently visited an exhibition of art and objects from the Jugendstil movement of the late 19th/early 20th century. Every piece was exquisite: purposeful, expressive, sinuously beautiful. 

<!--more-->

I was struck by the caption accompanying a selection of lamps from the collection. It mentioned that with the advent of electric lighting, the form of the lampshade was no longer constrained by stringent functional requirement of containing/protecting an open flame. Lampmakers, then, were free to experiment with the form to convey aesthetic imperatives.

It seemed wonderful to me that, liberated from functional constraints in this small domain by the advancement of science, artisans and engineers plowed all those degrees of freedom back into enhancing the beauty of the objects they designed. 

Do we still do this now, in the modern industrial age? I have the impression that when advances in engineering loosen functional constraints, we pocket the gains in efficiency directly as cash or allocate them to further improvements in performance. 

The idea of paying for beauty with science seems to have been tightly bound to the Jugendstil ethos. The use of science in the service of art seems ideological in a way that science in the service of efficiency, or further scientific progress, does not. This probably indicates a shift in our conception of the purpose of science. 

Of course Jugendstil was a movement, and did not represent the views of society at large. The idea of an art movement foregrounding art is not outlandish, although Jugendstil's ability to recruit talented engineers was impressive. It is still possible to find designers and artificers committed to using engineering to advance aesthetics: Apple, the most successful company of the modern era, is famously opinionated about design. Apple's aesthetic senses differs from that of the Jugendstil artisans, but is consistent and strongly held. The Jugendstil ethos is still here, it's just not evenly distributed.



![Magnolia Lamp - Louis Marjorelle/Daum Frères VMFA](/img/vmfa_magnolia_lamp_on.jpg)
