---
title: The config file for scientific progress
date: 2023-06-20
draft: false
tags: ["science", "metascience"]
cover: img/vienna-cafe.png
---
## Science is a people and a place at a time

Academia has lost some of its lustre lately, and  people are speculating about _What Comes After the Academy_.

I come not to bury the academy, but to examine it -- and maybe even to praise it. Why did we decide upon this particular institutional form to house our scientific apparatus? Are universities generating a good ROI on or research dollars?

<!--more-->

<center>
<figure style="text-align:center;">
<img src="/img/Civ3research.jpeg" width="400px"><figcaption>Not enough flasks to unlock the superconductor node in 2023</figcaption></figure></center>

Universities have been quite productive: most of the fundamental advances in science and social science made in the 20th century, for good or ill, can be traced to the campus.

There are a few peer institutions we can use as comparatives: the various national labs (BNL, LLNL,LANL, Fermilab, etc.) the Manhattan Project, Bell Labs, Xerox PARC, Lockheed's Skunkworks, Google, Deepmind, and OpenAI[^1]. But most of the public sector or commercial contenders for the Engine of Scientific Progress title are shorter-lived than Universities: Bell Labs was the longest lived, at ~80 years, while PARC was only active for three decades. The Manhattan Project was a goal-driven sprint.

I'd argue that  that the University -- particularly the American Land Grant University -- is one of the most robust institutions for driving scientific progress yet devised. I believe that its power derives from accommodating the preferences of a particular type of people, a type that disproportionately makes scientific contributions. The University is the natural "container" for these people.

A University is like a city for the life of the mind. As NYC specializes in finance, LA in movies, Nashville in music, and San Francisco in tech startups, a university is a unique community form creating agglomeration effects for the life of the mind, a Levittown for cognitive endeavors.

That's a neat trick! Why does it work?

<center>
<figure style="text-align:center;">
<img src="/img/Levittown.jpeg" width="600px"><figcaption>A planned community</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/Harvard.jpeg" width="600px"><figcaption>Also a planned community</figcaption></figure></center>

## How do universities work?

Successful innovation machines are always attached to a money generator: for the national labs, this is explicitly the government (although the government is a direct or indirect sponsor of many of these entities), while for the private sector entities are supported by an enterprise enjoying outsized financial success.

Having a monopoly or virtual monopoly (Bell Labs, Google) or having the USGOVT as your top client (Bell Labs, Skunkworks) is helpful, as is being deemed critical to national security, especially in times of existential threat (Bell Labs, the Manhattan Project, Skunkworks).

Money and monopoly are necessary but not sufficient to keep our innovation machines running. What makes them work is that these elite institutions become a Schelling Point for smart people.

Like early 20th century Vienna, Universities became known for being where interesting people congregate. Preferential attachment worked its magic as their reputations grew. Pretty soon they had a pretty sweet agglomeration effect going.

<center>
<figure style="text-align:center;">
<img src="/img/Vienna.png" width="600px"><figcaption>On second thought, let's not go to Vienna. It is a silly place.</figcaption></figure></center>

Anyone who has tried to recruit top researchers to the private sector knows that they can be more reluctant to leave than a simple analysis of compensation/workload would predict. Part of this is probably due to the irreversibility of the decision: academia is an up-or-out profession, like the military, and once you're out, it's difficult to get back in. But the qualities of the academic life itself also play a role.

Science is driven by certain configurations of certain people with certain characteristics and those people prefer academia to industry. Why is that?

## What is it about academia that scholars find so appealing?

The top draw has always been the presence of other scholars, their colleagues. These colleagues are often  also their neighbors, and opportunities for formal and informal socialization are more frequent than in many workplaces, even among other knowledge workers.
  
Part of the academic compensation package -- becoming a member of an ivory tower elite -- is very hard to emulate outside of the academy. Universities foster and promulgate an elite tribal identity (crests! scarves! mascots!) that has psychological and social benefits. Status is costly, and academics enjoy it at a discount. The physical surroundings of the university are often quite pleasant, with attractive housing and ample community services.

<center>
<figure style="text-align:center;">
<img src="/img/eatprestige.jpeg" width="300px"><figcaption>You can't eat prestige?</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/chestnut.jpeg" width="300px"><figcaption>Hold my diploma and watch this.</figcaption></figure></center>

Academics also enjoy long time horizons to work on ambitious projects, and are not bothered by mundane financial and bureaucratic details. The prospect of *tenure*, a permanent position and the freedom to define a research agenda, is the capstone incentive[^2].

Academics are surrounded by ambitious young people, and use their energy to create innovation flywheels. The presence of young people  also provides opportunities for mentorship, which is very rewarding to both junior and senior academics[^3].

## Can we replicate these features elsewhere?

This resembles one of the core questions of metascience: what are the factors driving  scientific progress? And how can we build institutions to accommodate and enhance them?

The obvious models for an innovation machine outside of the university system are the elite industrial labs of the past and present: Nela Park, Bell Labs, Xerox Parc, Skunkworks, Deepmind, Google, OpenAI, etc [^4].

Interestingly, not every tech giant with a money engine develops an academic-adjacent reputation: Google did, but Apple and Facebook largely did not, even though they are prestigious places to work, and Apple especially has a reputation for innovation. Here the outside view may be informative: unlike Google, Apple never really  developed a reputation for encouraging its SWEs to spend significant time on personal projects of uncertain business value ("20% time"). The impression that you can do unfettered blue-sky research without considering the bottom line is important. Openness to exploration is an important component of an innovation machine, and at some point on the explore/exploit curve institutions transition out of this space, even if they remain elite.

 It seems that an aspiring innovation machine must be willing to disrupt itself. It is not sufficient to merely become best in class, or the best version of itself, it must be willing to usher in something entirely new (LLMs disrupting search, or the nuclear era disrupting conventional superiority). An innovation machine must treat OKRs as a convenient myth.

The innovation machine blueprint includes an elite, aspirational identity that attract the best and most ambitious. It may be physically secluded and include a residential component fostering community and self-assured elite identity. It must be well-funded, with high-risk ventures on different timelines regularly throwing off impressive results -- even if these results are not the one initially envisioned by org leadership. And -- if we take the example of Bell Labs and the midecentury US university as examples -- this leadership should be of a piece with the research personnel, essentially ascended researchers with a strong research identity and orientation, still actively engaged in science and scientific mentorship[^5].

Academia was a Schelling Point for smart productive people. But all network effects are vulnerable to preference cascades. If enough elite performers decide that academia is no longer the best place to work, the network effect can unravel quickly. The creeping fear of every underpaid grad student is that those smart people are now somewhere else.

<center>
<figure style="text-align:center;">
<img src="/img/grandcentral.jpeg" width="300px"><figcaption>Where to next?</figcaption></figure></center>

[^1]: Institutions like DARPA and ARIA are probably best understood as an overlay network atop a university system.

[^2]: It is the unravelling of this part of the bargain, with the focus on grants, service, and publications that is threatening the entire academic edifice. Academics no longer enjoy such long time horizons to plan their research, or the trust of their university leadership to deliver results "when they're ready". The bureaucratization of the academy may have  secondary effect of selecting for researchers less bothered by these requirements and having a lower appetite for risk, who prize tenure not for the ability to set an ambitious agenda or speak uncomfortable truths, but rather for the job security.

[^3]: The presence of young people also provides opportunities for adulation and coercion, especially sexual coercion. Despite their woke trappings, universities are far behind the private sector in rooting out sexual harassment and hostile working environments.

[^4]: There might be some circularity inherent in this comparison, as some of the more recent innovation machines consciously modeled themselves after the academy: long-time Googlers, for instance, have explicitly compared the atmosphere of the company to grad school. References to "parks" and "campuses" abound in elite tech innovation spaces.

[^5]: This organizational form could probably be extended to the humanities as well with some modifications: elite, enormously productive artistic communities have have been an important driver of cultural florescence.