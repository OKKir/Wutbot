---
title: "Station Hope"
template: "page"
socialImage: "/img/Wutbot_01.png"
date: 2024-01-02
---
## Concept

>"Every step of the way is a decision"

*Station Hope*  is a narrative-driven interactive fiction experience exploring the challenges facing freedom seekers navigating the Underground Railroad  Set after the passage of the Fugitive Slave Act of 1850, most of the game takes place in the state of Ohio

The player character is  a (fictional) freedom seeker modelled after  *{insert historical  model(s)}*  leaving a plantation in *{origin location TBD, probably western Virginia}* to cross the Ohio River. Your goal is to reach *Station Hope* -- Cleveland, Ohio -- and passage to freedom in Canada.

#### Gameplay

The goal of *Station Hope* is to immerse players in a roleplay experience that viscerally conveys the hardships endured and challenges overcome  by  black Americans escaping slavery.  Players should finish the game admiring freedom seekers in the way they admire the members of the Fellowship of the Ring, or Lewis and Clark, or the Knights of the Round Table, or Maximus of *The Gladiator*.

<div style="text-align: center;">
<center>
<figure style="text-align:center;">
<img src="/img/ugrr_still_book.png" width="600px" style="display: block; margin: 0 auto;"><figcaption>Station Hope will emphasize "hair-breadth escapes"  and "death struggles" like those recorded in 
<a href="https://www.google.de/books/edition/The_Underground_Rail_Road/8ANWAAAAcAAJ?hl=en&gbpv=0">William Still's book</a></figcaption></figure></center>
</div>

The game will be grounded in historical research [^1] but will take liberties with gameplay elements like specific puzzles and dilemmas: the aim is to simulate the emotional and intellectual effort needed to survive the quest for freedom rather than reconstruct a particular historical narrative.

*Station Hope* will contain ~1-3 hours  of total gameplay, with a  run-through taking ~1h on average to complete.

The player will advance through the story by making choices and solving problems using reason and real-world knowledge. Knowledge of the period may be helpful, but is not necessary to complete the journey.

The player character will be able to acquire and use a small inventory of useful items along their journey, including clues helping to guide them along their way. The game will  begin with a brief  chapter at the home plantation that serves as a short tutorial and introduction to the game world, where sympathetic NPCs (non-player characters) provide the player character with a few critical items.

The *Station Hope* experience will include meaningful choices that affect how the world reacts to the player character, and the skills that the player character  develops during their journey. There will be a vey limited set of statistics calculated "underneath the hood" of the game engine and largely hidden from the player governing these interactions. There will also be a few (2-3) basic statistics affecting gameplay that the player will have to manage along with their inventory, such as their stamina level or charisma.

##### Potential gameplay elements

- Persuasion and misdirection games  during  NPC encounters to distinguish friend and foe
- a celestial navigation element ("follow the drinking gourd")
- a terrestrial or compass navigation element
- simple crafting  puzzles (i.e. create a compass, create a needed tool, make a stew, light a fire)
- if a non-literate historical model is chosen for the player character, puzzles related to interpreting maps, or signposts (symbol interpretation), or hidden messages in songs
- memory puzzles
- decipherment and matching puzzles
- botany and medicinal puzzles

A  subset of these elements will be used to create a *gameplay helix*, where the player character masters  different basic gameplay elements and combines them  to solve  problems of  differing complexity as they progress through the game world.

##### Success and Failure Conditions

While the gameplay loop  itself will include fictionalized characters and scenarios, the choices the player makes at each decision point, and the successful /unsuccessful resolution of puzzles  will unlock relevant factual historical data. These stories and tidbits will be collected in the player's in-game  journal. 

For example, successfully navigating northward using the stars might unlock a journal entry on  "The Drinking Gourd", while  successfully evading a bounty hunter might reveal a story of a similar exploit by a real escapee. The player "wins" by collecting the greatest number of story fragments, filling the pages of their journal. The game's story will reveal that the player character became a successful community member and griot retelling these stories  in Canada, where they guide others on their quest.

Successful choices will reveal more of the story, but there are some narratives that will only be  unlocked by "failure": for  instance, recapture by a bounty hunter might reveal the story of [Lucy Bagby Johnson](https://clevelandhistorical.org/items/show/517).  "Failure" is tricky: the goal is for the player  to experience as much of the content as possible, and for  the consequences of most failures to be temporary and non-fatal, only affecting the level of hardship or suspense the player endures.

It is important that even the "worst" failure condition, capture by a slave hunter, should convey that a second chance at escape is possible, and the freedom seeker's indomitable spirit will urge them to take up the quest again.  At this point, I do not envision "death"  as a possible player failure condition, although some particularly perilous moments could  unlock stories about fugitives who perished  *en route*.

#### Aesthetic and User Interface

##### Art Style

The game will use 2D  hand-drawn  illustrations, including "paper doll" style rigged models for simple animations ([example of paper doll animation from *Pentiment*](https://indiegamesource.tumblr.com/post/711024861130178560).) Most images in the game will be static, with limited animations (changes in expression, simple limb movements). Puzzles may be simply animated (moving shapes, etc.). 

This is in contrast to the pixel art 2D style familiar to players from e.g. *Super Mario Brothers* or the 3D styles popular in modern AAA video game titles. Hand-drawn animation is faster to produce and results in a smaller overall game file size than either 2D or 3D pixel art. It  is also a better fit to  *Station Hope*'s  gestalt as an interactive book  + accompanying map (a digital *Choose Your Own Adventure* book).

<center>
<figure style="text-align:center;">
<img src="/img/cyoa.jpg" width="500px" style="display: block; margin: 0 auto;"><figcaption>Image from a "Choose Your Own Adventure" gamebook, where players make choice and turn to the corresponding page to  advance the story/see the result of their choice. Both text & illustrations  can  contain clues.</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/potion_craft.jpg" width="600px"><figcaption>Examples of 2D hand-drawn video games: "Potion Craft" (above), "Pentiment" (below) </figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/pentiment02.png" width="600px"><figcaption>Examples of 2D hand-drawn video games: "Potion Craft" (above), "Pentiment" (below) </figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/imrs.jpg" width="600px"><figcaption>The art  style of the  popular hand-drawn game "Pentiment" is both charming and quite flexible. </figcaption></figure></center>

I'm experimenting with illustration styles based on artists and illustrators from the period, like Thomas Nash, Winslow Homer,  and Currier & Ives.

<center>
<figure style="text-align:center;">
<img src="/img/station-hope01.png" width="600px"><figcaption>1st-person PoV renderings of environments such as forests and swamps will be central to the game art</figcaption></figure></center>

##### User Interface

The game will use a minimal set of screen designs. Most gameplay will take place on  layered interface:
 the bottom layer is the game map, the second layer is a simple illustration of the current problem or conversation, and the top layer consists of text boxes containing dialogue and player choices.
 
- The map can be viewed as a standalone screen
- The griot's journal can be reviewed: this item exists outside of the game world for the player rather than the player character
- The inventory screen can be checked when items are needed
- There will also be a separate section for clues or important bits of information (if the player character is not literate, this screen will represent memorized items).
 
<center>
<figure style="text-align:center;">
<img src="/img/sorcery_03.png" width="500px" style="display: block; margin: 0 auto;"><figcaption>This screen from Sorcery! shows how the player experiences most of the game: an illustration of the current scenario, a short block of narrative, and a set of player choices. Clicking on a choice advances the story. The presentation of story choices in Station Hope will resemble this example. </figcaption></figure></center> 

<center>
<figure style="text-align:center;">
<img src="/img/pentiment-map.jpeg" width="600px" style="display: block; margin: 0 auto;"><figcaption>At the resolution of each scenario, the player can see how the player character's avatar has advanced along the map (example map from Pentiment)</figcaption></figure></center> 
 
<center>
<figure style="text-align:center;">
<img src="/img/sorcery_map.png" width="600px" style="display: block; margin: 0 auto;"><figcaption> The correct orientation of the map in Station Hope will be one of the puzzles, and parts of it  will be obscured. The map will be gradually pieced together from clues that the player character weaves together (example map from Sorcery!)</figcaption></figure></center> 

<center>
<figure style="text-align:center;">
<img src="/img/pentiment-journal.png" width="550px" style="display: block; margin: 0 auto;"><figcaption> The griot's journal will be a text representation of the stories and fact the narrator has learned -- it will resemble a storybook. There will also be a screen for reviewing clues and information the player has collected during their quest. These clues may be depicted as memories in-game</figcaption></figure></center> 

#### Technology

This game will use off-the-shelf  free and open-source technology wherever possible. 

Art assets can be created in Krita and Procreate and rigged in Blender.

Most of  *Station Hope* will be scripted in [Ink](https://www.inklestudios.com/ink/), the dialogue engine used to create  *Sorcery!* and other works of interactive fiction.  

Ink can be combined with game engines like [Unity](https://github.com/inkle/ink-unity-integration) or [Godot](https://github.com/paulloz/godot-ink) using open-source integration tools (*Sorcery!* was made in Ink + Unity ). Using Godot, an open-source game engine,  and Ink removes risks around engine software costs (Unity has an unclear subscription scheme) but introduces a bit of hassle when porting to formats like mobile. 

This decision can be deferred until the scripting in Ink is largely complete. The important point is that the tools needed to create the game are free or inexpensive until we reach the stage where the game is exported to a mobile (iOS or Android) format, and a devkit is required. Devkits are  <$1000. 

## Roadmap and de-risking

#### Development

The game that *Station Hope* will be most closely modelled after, *Sorcery!*, contains ~150,000 words in roughly 200 word chunks, or scenes, giving rise to over 2,600 player choices. 

*Sorcery!* benefitted from being able to draw directly from a CYOA gamebook and its existing artwork. It was created by a two man team of experienced game designers, and they contracted the additional required art (NPC avatars, UI elements, etc.)  and music to freelancers. Inkle,  the developers behind *Sorcery!*, state that each title in the series (there are 4) took about a year to make and  cost  [~ £10k / title, not including salaries](https://www.gamedeveloper.com/business/postmortem-i-steve-jackson-s-sorcery-i-series-by-inkle.).

At this point, I haven't decided whether  to develop *Station Hope*  as mobile game for iOS, playable on iPhones and  iPads, or for PC.  Mobile development is difficult, but would allow e.g. museumgoers  to download the app to play on their personal devices -- as long as they have Apple devices (developing for Android would  extend the scope of the project too much for a single dev).  It is easier to develop for PC, but a Windows game is a less intimate experience than mobile for a short gamebook.

 I've decided to break *Station Hope*'s content into chapters to enable iterative development:
 
1. __Chapter One__ will include the parts of the story taking place at the origin plantation up to the crossing of the Ohio River.  This chapter will be relatively short -- <50,000 words, or about 500 player choices (some major, some trivial)
2. __Chapter Two__  will include the flight through Ohio to Cleveland.  This will be ~100,000 words of content, or about  ~2000 player choices (some major, some trivial)

Chapter One is much shorter than Chapter Two, but the creation of Chapter One will require about 60% of the effort needed to complete the entire game, as the art style, game mechanics, game story, etc. have to be finalized during this phase. Most of the time savings from splitting the effort into phases will be from the shorter game script and reduced number of illustrations.

The iterative development plan for *Station Hope* increases the chance of successfully delivering a completed game.  The MVP is about 1/4 the complexity of the project that I think I can complete in one year; according to my heuristic for software estimation I can be pretty confident (>95%) of completing the Stage 1 deliverables in 12-15 months. I'd estimate there's about a 30% chance of me completing the entire project (Stages 1-4) in one year, and 90% chance of finishing in two years.

1. 🛹 The __Minimum Viable Product__ is a complete game  story  outline + Chapter One of the game  as a web  browser experience  (game plays from an internet site).   The griot's journal is not illustrated. 
   
2. 🛴 Stage 2 is a complete story outline + Chapter One as a mobile game for browser + iOS.
   
3. 🚴 Stage 3  is Chapter One as a mobile game for iOS and   Chapters One and Two (the complete game)  as a browser experience.
   
4. 🚗 Stage 4 is the complete game available for the browser and iOS. The griot's journal is fully illustrated.

The most likely failure point is the integration of the Inkscript with Unity/Godot for a mobile experience; the remedy is to pivot to a browser-based game, which limits some of the possible gameplay mechanics, but is sufficiently full-featured for a rich interactive fiction experience.

Each stage of development produces at least one finished product that can be used to build excitement for the final product, attract collaborators or additional investment, if needed, and deployed to museums and interpretive  centers.

#### Additional  Materials

- The griot's journal can be produced as a printed book for use in educational settings, or a as a gift to financial supporters, [Kickstarter](https://www.kickstarter.com/)-style.
- 
- The development log  will be recorded  and uploaded to the [web ](https://ask.pubpub.org/)as a public [Discourse Graph](https://oasis-lab.gitbook.io/roamresearch-discourse-graph-extension/fundamentals/what-is-a-discourse-graph)so that interested students, educators, and developers can reconstruct how and why certain design decisions were made.

## Learning from previous games with an Underground Railroad theme 

- [Freedom](https://www.youtube.com/watch?v=zJoM7K1DkyM):  a cooperative  [board game](https://boardgamegeek.com/boardgame/119506/freedom-underground-railroad) where you play as a conductor &  abolitionist leading slaves to freedom (2013)
	- available ✅
	- commercial
	- looks fun
	- Kickstarted
- [We Walked in Darkness](https://store.steampowered.com/app/766090/We_Walked_In_Darkness/): 2.5D side-scrolling [video game](http://cellecgames.com/we-walked-in-darkness/) with puzzle elements  where you play as a freedom seeker (2018)
	- available ✅
	- quasi-commercial: available on Steam, but developed by an educational developer
- Freedom:   Apple II  [video game](https://www.vice.com/en/article/3annjy/the-oregon-trail-studio-made-a-game-about-slavery-then-parents-saw-it)  from the makers of Oregon Trail with  similar adventure game mechanics  where you play as a freedom seeker (1992)
	- no longer available ❌
	- commercial for an educational market
- [The Underground Railroad: Journey to Freedom](https://media.nationalgeographic.org/assets/file/Underground_Railroad_Educator_Guide.pdf): 3D video game for web & tablet developed for National Geographic  where you play as a freedom seeker(2014)
	- doesn't seem to be currently available ❌
	- nice pdf connecting gameplay to nat'l historical education standards  and principles 
	- not commercial, commissioned by NatGeo
	- 20-30 minutes of gameplay total 
- [Mission US: Flight to Freedom](https://www.mission-us.org/) : browser-based [video game](https://www.gamesandlearning.org/2015/04/01/slavery-game-controversy-raises-questions-around-history-games/)  with an underground railroad module  where you play as a freedom seeker (fully voiced CYOA) (2014)
	- [controversial](https://current.org/2017/11/second-school-district-halts-use-of-wnets-mission-us-games/)
	- currently under  re-development & unavailable ❌
	- very expensive ($786k!) to produce!
	- 2m players over all games in the series
	- $20k going into the next game in the series, about the civil rights era in MS,  from the NEH (seems a bit low?)
	- not commercial, commissioned by  the Corporation for  Public Broadcasting

➡️all of these games involve moving  large distances across a map (the map is implied in the side-scrolling game)[^3]

➡️ commercial games are more likely to remain available years after launch -- the educational games just ... disappear

➡️ this game topic, even when handled very well, as in the *Freedom* board game, can make some players uncomfortable (see this [review](https://www.youtube.com/watch?v=ISfKs2ms35M))

↪️avoid controversy by *not* positioning as a product for schools, allow interested players to self-select by rolling out either commercially or in selected venues like museums or interpretive centers

↪️Design and market the game for adults, but keep it appropriate for kids

<center>
<figure style="text-align:center;">
<img src="/img/freedom_game.png" width="400px"><figcaption>2D pixel art from the  1992 "Freedom" computer game</figcaption></figure></center>

<center>
<figure style="text-align:center;">
<img src="/img/jtf_game.png" width="300px" style="display: block; margin: 0 auto;"><figcaption>Example of 3D pixel game art from "Journey to Freedom"</figcaption></figure></center>


## Comps and influences

These games have nothing to do with slavery or the UGRR but have relevant aesthetics or gameplay mechanics, and demonstrate existing demand for  experiences similar to *Station Hope*:

#### Gameplay inspiration and proof-of-concept

<div style="text-align: center;">
<center>
<figure style="text-align:center;">
<img src="/img/A_map.jpg" width="400px" style="display: block; margin: 0 auto;"><figcaption>Sorcery! game map showing character avatar, stats,  and quest markers</figcaption></figure></center></div>

[Sorcery!](https://www.inklestudios.com/sorcery/) by Inkle is a series of Fighting Fantasy CYOA gamebooks turned into mobile/PC/console games
	- developed from a game book series published in the 1980s
	- *Station Hope* will use  the  open-source dialogue engine developed by this studio
	- 1-3 hours of playtime for the 1st  *Sorcery!* installment
		- 150,000 words
		- 795 scenes
		- 2,600 player choices
		- over 30 original art scenes
		
*Sorcery!* is the primary inspiration for this project, and the gameplay of *Station Hope*  will follow the *Sorcery!* model. *Sorcery!* is a map-based  choose-you-own-adventure gamebook-turned mobile game: the player avatar moves over a map as they make text-based choices in which they  encounter enemies and allies, have conversations, solve puzzles, use magic, and even engage in turn-based combat (like in a tabletop rpg).

*Station Hope* will not include combat or magic, but it will use similar mechanics to make choices during difficult conversations, navigate the map, and solve puzzles. As in *Sorcery!*, the player character will navigate the map via their choices, rather than  by moving a map marker directly.  The experience should reflect the fact that most freedom-seekers did not have a map at their disposal, while giving the players a sense of the magnitude of the challenge. All of the action is text-based and may require cogitation or intuition, but not quick reflexes.

↪ *Station Hope* will have to find the right balance of informing and engaging the player character while conveying a truthful experience

<div style="text-align: center;">
<center>
<figure style="text-align:center;">
<img src="/img/sorcery_interior.png" width="300px"><figcaption>An interior in Sorcery!</figcaption></figure></center></div>

#### Games illustrating demand for rich conversational experiences

- [Coffee Talk](https://store.steampowered.com/app/914800/Coffee_Talk/) (2021)
	- 4 hour playthrough, 8h  total content
	- demonstrated demand for games with lots of conversation and minimal gameplay
- [Tavern Talk](https://taverntalkgame.com/) a visual novel game (2023)
	- 10h of content
	- like Coffee Talk but with slightly more gameplay
	- 20k dev goal (160k raised)
	
<center>
<figure style="text-align:center;">
<img src="/img/coffee-talk01.jpeg
" width="600px"><figcaption> Coffee Talk</figcaption></figure></center> 

These games are mostly conversation, with very little gameplay, and not very much player choice (the story is advanced by serving different drinks to coffeeshop patrons). They are much more like visual novels. Coffee Talk was very successful, and received a sequel, while *Tavern Talk*'s Kickstarter has exceeded expectations. ➡️ Rich narrative-first experiences are in demand [^2].

<center>
<figure style="text-align:center;">
<img src="/img/kim-handshake.jpeg
" width="600px"><figcaption> Disco Elysium</figcaption></figure></center> 

[Disco Elysium](https://store.steampowered.com/app/632470/Disco_Elysium__The_Final_Cut/) was a smash hit, largely on the strength of its narrative-based gameplay. Its scope and artistic and technical complexity are beyond what we're planning for Station Hope, but it does signal an appetite for narrative-heavy games  with text-based interactions.

#### Games illustrating demand for a well-research historical setting

<center>
<figure style="text-align:center;">
<img src="/img/pentiment03.png" width="600px"><figcaption> Pentiment</figcaption></figure></center> 

[Pentiment](https://pentiment.obsidian.net/) is not an educational game, but it executed its setting and aesthetic so well that players couldn't help but learn about the period, and many had their curiosity piqued enough to  explore it further: Obsidian minted at least a few new Umberto Eco fans.

#### Games illustrating demand for meaningful & difficult choices

[Roadwarden](https://moralanxietystudio.com/) is a succesful indie game with a subdued pixel art aesthetic that really nailed *tradeoffs* and *scarcity*. This imbued player choices with heightened meaning, and added a layer of realism to its fantasy setting. *Station Hope* can learn from its economy design.


<center>
<figure style="text-align:center;">
<img src="/img/roadwarden.jpg" width="600px"><figcaption> Pentiment</figcaption></figure></center> 


---- 

<center>
<figure style="text-align:center;">
<img src="/img/underground railroad image.jpg
" width="600px"><figcaption> The real Underground Railroad  was the choices freedom seekers made along the way</figcaption></figure></center> 


[^1]: Sources include *Bound for Canaan* by Fergus Bordewich, *The Underground Railroad: from slavery to freedom* by William Siebert, *Liberty Line* by Larry Garra, *Gateway to Freedom* by Eric Foner, and *The Underground Railroad: a record* by William Still
[^2]: One caveat is that these games are in a "cozy" genre advertised as being high-aesthetic and low-stakes
[^3]: I am aware of one *Choose Your Own Adventure*-style book with an UGRR theme: *"The Underground Railroad"* by A. Lassieur. It is generally positively reviewed, which suggests there is demand for this type of experience, but 1.) it is targeted to children, while we plan to create an experience targeted to adults but suitable for children, and 2.) only one of the three short storylines centers a freedom seeker; the other two follow an abolitionist and a slave catcher
