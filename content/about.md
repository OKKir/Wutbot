---
title: "About me"
template: "page"
socialImage: "/img/Wutbot_01.png"
---

Lab assistant to the famed Dr Light. ![Meet the new boss](/img/drlight_01.jpg#thumbnailhangleft)

 ![Same as the old boss](/img/dr_wily_01.png#thumbnailhangright)

Former postdoc of the infamous Dr Wily.

Have hand cannon, will travel.