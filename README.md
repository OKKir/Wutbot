![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---
## From the Gitlab Pages Example Hugo Site

 ![Hugo](https://gohugo.io/) website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

## About

This a a Hugo blog using a modified version of the [Hello Friend](https://github.com/panr/hugo-theme-hello-friend/) theme.

It was built from the [Gitlab Example Hugo Site](https://gitlab.com/pages/hugo), which uses the [Beautiful Hugo](https://pages.gitlab.io/hugo/) theme.

## Theme Modifications

This version adds a sidebar based on the [Hyde](https://github.com/spf13/hyde) theme with a personal avatar, social links, menu, and tag list.

It also explicitly adds parameters to [`config.toml`](.config.toml) to canonify urls and add relative and ugly urls to enable local disk previewing and easy image linking on Gitlab and Github pages.

## Style

This theme also adds a bit of __css__ styling to the __markdown__ blog posts. 

Instead of shortcodes, where possible overloaded `src` and `alt` attributes are used as psuedo-classes and selectors. 